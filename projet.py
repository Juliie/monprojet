##__________________1________________________
from time import sleep #on importe la bibliothèque sleep de time
from easygui import ccbox #on importe easygui
import sys
#________________2_________________________
#Compte à rebours
for sec in range(10, 0, -1):  #Pour les secondes allant de 10 à 0 en reculant de 1
    print(sec, '*'*sec)  #afficher la seconde avec '*' fois la seconde
    sleep(1) #affiche toutes les 1 seconde le résultat, le programme est mis en pause pendant n seconde
    
    
msg = "Tu veux continuer?" # interface graphique easygui qui demande si l'on veut continuer ou arrêter
titre = "Merci de confirmer"
if ccbox(msg, titre):
    pass
else:
    sys.exit(0) #sinon quitter

les_filles = [

('Léonie', 153),

('kily', 174),

('Anne', 167),

('Ysaline', 166),

('Julie', 165),

]
# fonction qui calcule le nombre de personne dans la liste
def nb(filles: list) -> int:
    compteur = 0
    for nb in filles:
        compteur += 1
    return compteur
assert nb([]) == 0, 'Il y a un problème avec la liste vide'

#fonction qui calcule la moyenne des tailles de la liste
def moyenne_taille(filles: list) -> int:
    assert filles != [], "Il n'y a personne donc pas de moyenne!"
    compteur = 0
    som_taille = 0
    for taille in filles:
        compteur += 1
        som_taille += taille
    return som_taille // compteur
   #___________________3__________________________
print "La somme des filles est :", str(nb(filles))
print "la moyenne des tailles des filles est :", str(moyenne_taille(filles))