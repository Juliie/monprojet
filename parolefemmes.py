Quelle radio publique a offert le plus petit taux de parole aux femmes en 2019 et quel était ce taux ?



Programme python permettant de trouver la réponse : 


from urllib.request import urlretrieve
import csv

fichier_paroles_csv , _ = urlretrieve('https://raw.githubusercontent.com/Informathix/2nde/master/2019_20/Projet2_Donnees/ParoleFemmes.csv')
lecteur_paroles = csv.DictReader(open(fichier_paroles_csv,'r', encoding='utf8'))
paroles = [dict(ligne)for ligne in lecteur_paroles]
maxi = max(
    {
        (ligne['women_expression_rate'], ligne['channel_name'])
        for ligne in paroles
        if ligne['media_type'] == 'radio'and ligne['year'] == '2019'
    }
)
print(maxi)


C’est Chérie FM qui a offert le plus petit taux de parole aux femmes en 2019 avec 48.675315574656416 %.